# Switch, ternary

## Ternary operator

```java

int result = condition ? 1 : 0;

```


## Switch

### prior Java 12
```java
int no = 3;
switch(no) {
    case 1:
        System.out.println("1");
    case 3:
        System.out.println("3");
        break;
    case 4:
        System.out.println("4");
    default:
        System.out.println("which one?");
}
```

### after Java 12
```java
int no = 3;
switch(no) {
    case 1, 3 -> System.out.println("1");
    case 3    -> System.out.println("3");
    case 4    -> System.out.println("4");
    default   -> System.out.println("which one?");
}
```

### after Java 13
```java
int no = 3;
switch(no) {
    case 1:
        System.out.println("1");
    case 3:
        System.out.println("3");
        break;
    case 4:
        System.out.println("4");
default:
        System.out.println("which one?");
        }
```

# Task
1. go to https://gitlab.com/gregmazur/basic-java and fork the repository
2. `git clone https://gitlab.com/<your-name>/basic-java.git`
3. go to the `02_Primitives_operators_if_else` dir
4. `git checkout -b 02_Primitives_operators_if_else`
5. create a directory called `examples`
6. **Add files with examples of:**
    - **ternary operator**
    - **switch in every option**

Files should be ready to compile like:
```java
package examples;

public class NameOfTheFile {
    public static void main( String[] args ) {
        //your code
    }
}
```

*NameOfTheFile should be changed
